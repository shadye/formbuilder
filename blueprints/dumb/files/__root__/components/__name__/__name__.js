import React from 'react'
import styles from './<%= pascalEntityName %>.css'

type Props = {

};
export class <%= pascalEntityName %> extends React.Component {
  props: Props;

  render () {
    return (
      <div className={styles.container}></div>
    )
  }
}

export default <%= pascalEntityName %>

