import React from 'react'
import FlatButton from 'material-ui/FlatButton'
import styles from './WButton.css'
import classnames from 'classnames'

type Props = {
  className: React.PropTypes.string,
  label: React.PropTypes.node.isRequired,
  handler: React.PropTypes.func
};
export class WButton extends React.Component {
  props: Props;

  render () {
    //override for inline styles
    let FBStyle = {
      borderRadius: '0.3em',
      width: '160px'
    }
    return (
      <div className={classnames(this.props.className, styles.button)}>
        <FlatButton
          style={FBStyle}
          label={
            <span className={styles.label}>{this.props.label}</span>
          }
          onTouchTap={this.props.handler}/>
      </div>
    )
  }
}

export default WButton

