import React from 'react'
import Sortable from 'react-sortablejs'
import Editable from 'react-contenteditable'
import Checkbox from 'material-ui/Checkbox'
import FlatButton from 'material-ui/FlatButton'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import styles from './DraggableList.css'
import classnames from 'classnames'

export const DraggableList = ({items, onChange, editField, removeField}) => {
  const addChoice = (key, choices) => {
    editField({index: key, delta: { choices: [...choices, ''] }})
  }
  const editChoice = (key, choices, choiceIndex, choiceDelta) => {
    choices[choiceIndex] = choiceDelta
    editField({index: key, delta: { choices: choices }})
  }
  const removeChoice = (key, choices, choiceIndex) => {
    choices.splice(choiceIndex, 1)
    editField({index: key, delta: { choices: choices }})
  }
  const renderChoices = (type, choices, key) => {
    switch (type) {
      case 'text':
        return <input
          className={styles.choices_text}
          type='text'
          disabled
          placeholder='Single-line text'
          value={choices} />
      case 'radio':
      case 'checkbox':
        return <div>
          {
            choices.map((c, i) => {
              return <div className={styles.choices}>
                <input
                  className={styles.choices_multi}
                  disabled
                  type={type}
                  value={c} />
                <Editable
                  className={styles.choices_multi_edit}
                  html={c}
                  onChange={e => editChoice(key, choices, i, e.target.value)} />
                <DeleteIcon
                  className={styles.choices_multi_remove}
                  onTouchTap={removeChoice.bind(null, key, choices, i)} />
              </div>
            })
          }
          <span
            className={styles.choices_add}
            onTouchTap={addChoice.bind(null, key, choices)}>
            + Add choice
          </span>
        </div>
      case 'select':
        return <div>
          {
            choices.map((c, i) => {
              return <div className={styles.choices_multi}>
                <Editable
                  className={styles.choices_multi_edit}
                  html={c}
                  onChange={e => editChoice(key, choices, i, e.target.value)} />
                  <DeleteIcon
                    className={styles.choices_multi_remove}
                    onTouchTap={removeChoice.bind(null, key, choices, i)} />
              </div>
            })
          }
          <span
            className={styles.choices_add}
            onTouchTap={addChoice.bind(null, key, choices)}>
            + Add choice
          </span>
        </div>
      case 'file':
        return <input
          className={styles.choices_text}
          type='file'
          disabled
          placeholder='File upload'
          value={choices} />
      case 'ptext':
        return <textarea
          className={styles.choices_text}
          disabled
          placeholder='Paragraph text'
          value={choices} />
    }
  }
  const listItems = items.map((val, key) => (
    <div key={key} data-id={key} className={styles.row}>
      <span className={styles.row_mover}>&nbsp;</span>
      <div className={styles.row_title}><Editable html={val.title} onChange={e => editField({index: key, delta: {title: e.target.value}})} /></div>
      <div className={styles.row_choices}>
        {renderChoices(val.type, val.choices, key)}
      </div>
      <div className={styles.row_required}>
        <Checkbox
          className={styles.checkbox}
          defaultChecked={val.isRequired}
          onCheck={(e, isCheck) => { editField.call(null, {index: key, delta: {isRequired: isCheck}}) }} />
        <FlatButton
          className={styles.remove}
          label='Remove'
          onTouchTap={removeField.bind(null, key)}/>
      </div>
    </div>)
  )

  return (
    <div>
      <div className={styles.row}>
        <span className={classnames(styles.row_title, styles.row_title_header)}>QUESTION TITLE</span>
        <span className={classnames(styles.row_choices, styles.row_choices_header)}>CHOICES</span>
        <span className={classnames(styles.row_required, styles.row_required_header)}>REQUIRED?</span>
      </div>
      <Sortable
        onChange={(order, sortable) => {
            onChange(order)
        }}
        >
        {listItems}
      </Sortable>
    </div>
  )
}

DraggableList.propTypes = {
  items: React.PropTypes.array,
  editField: React.PropTypes.func,
  onChange: React.PropTypes.func
}

export default DraggableList
