import React from 'react'
import styles from './Preview.css'
import WButton from 'components/WButton'
import DraggableList from 'components/DraggableList'
import _ from 'lodash'

type Props = {
  editField: React.PropTypes.func.isRequired,
  replaceField: React.PropTypes.func.isRequired,
  removeField: React.PropTypes.func.isRequired,
  passNotUniqTitlesError: React.PropTypes.func.isRequired,
  passEmptyTitleError: React.PropTypes.func.isRequired,
  passNotUniqChoicesError: React.PropTypes.func.isRequired,
  clearErrors: React.PropTypes.func.isRequired,
  form: React.PropTypes.array.isRequired,
  description: React.PropTypes.string.isRequired,
  errors: React.PropTypes.array.isRequired
};
export class Preview extends React.Component {
  props: Props;

  constructor (props) {
    super(props)
    this.messages = {0: 'Titles must be uniq', 1: 'Titles must be not empty', 2: 'Choices must be uniq'}
    this.state = {
      errors: {0: '', 1: '', 2: ''}
    }
  }

  passError (testsResults) {
    let newErrors = {}
    testsResults.map((test, i) => newErrors[i] = test ? this.messages[i] : '')
    if (!_.keys(_.omitBy(newErrors, _.isEmpty)).length) this.saveForm()
    this.setState({
      errors: newErrors
    })
  }

  testUniqTitles (form) {
    return _.uniqBy(form, 'title').length !== form.length
  }

  testEmptyTitles (form) {
    return _.reduce(form, (haveEmpty, field) => {
      if (field.title.length === 0) haveEmpty = true
      return haveEmpty
    }, false)
  }

  testUniqChoices (form) {
    return _.reduce(form, (collisions, field) => {
      if (_.uniq(field.choices).length !== field.choices.length) collisions = true
      return collisions
    }, false)
  }

  validateForm () {
    const { form } = this.props
    this.passError([this.testUniqTitles(form), this.testEmptyTitles(form), this.testUniqChoices(form)])
  }

  saveForm () {
    window.localStorage.setItem('redux form', JSON.stringify(this.props.form))
  }

  render () {
    const { form, description, editField, removeField, replaceField } = this.props
    const { errors } = this.state
    let errorContainer
    if (_.keys(_.omitBy(errors, _.isEmpty)).length) {
      errorContainer = <div className={styles.preview_errors}>
        {Object.values(errors).map((error, i) => <span key={i}>{error}</span>)}
      </div>
    }
    return (
      <div className={styles.container}>
        <div className={styles.preview}>
          <div className={styles.preview_header}>
            <span className={styles.preview_header_logo}>San Francisco Driver Form</span>
            <WButton
              className={styles.preview_header_save}
              label='Save form'
              handler={::this.validateForm} />
          </div>
          { errorContainer }
          <div className={styles.preview_description}>
            <span className={styles.preview_description_title}>description:
            </span>
            <span className={styles.preview_description_data}>{description}</span>
          </div>
          <DraggableList
            items={form}
            onChange={items => { replaceField(items) }}
            editField={editField}
            removeField={removeField} />
        </div>
      </div>
    )
  }
}

export default Preview
