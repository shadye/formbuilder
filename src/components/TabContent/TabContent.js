import React from 'react'
import styles from './TabContent.css'

type Props = {
  logo: React.PropTypes.string.isRequired,
  desc: React.PropTypes.string.isRequired
};
export class TabContent extends React.Component {
  props: Props;

  render () {
    return (
      <div className={styles.container}>
        <div className={styles.container_desc}>
          {this.props.desc}
        </div>
        <h3 
          className={styles.container_header}>
          {this.props.logo}
        </h3>
        {this.props.children}
      </div>
    )
  }
}

export default TabContent

