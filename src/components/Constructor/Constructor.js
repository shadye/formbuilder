import React from 'react'
import styles from './Constructor.css'
import Panel from 'components/Panel'
import Preview from 'components/Preview'

type Props = {
  addField: React.PropTypes.func.isRequired,
  editField: React.PropTypes.func.isRequired,
  replaceField: React.PropTypes.func.isRequired,
  removeField: React.PropTypes.func.isRequired,
  changeDescription: React.PropTypes.func.isRequired,
  form: React.PropTypes.array.isRequired,
  description: React.PropTypes.string.isRequired
};
export class Constructor extends React.Component {
  props: Props;

  render () {
    const { addField, changeDescription } = this.props
    return (
      <div className={styles.container}>
        <Panel addField={addField} changeDescription={changeDescription} />
        <Preview {...this.props} />
      </div>
    )
  }
}

export default Constructor

