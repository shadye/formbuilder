import React, { Component, PropTypes } from 'react'
import styles from './Description.css'

export class Description extends Component {
  onDescriptionChange (e) {
    this.props.changeDescription(e.target.value)
    e.stopPropagation()
  }

  render () {
    return (
      <textarea onChange={::this.onDescriptionChange} className={styles.textarea} />
    )
  }
}

Description.propTypes = {
  changeDescription: PropTypes.func.isRequired
}

export default Description

