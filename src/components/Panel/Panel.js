import React from 'react'
import styles from './Panel.css'
import {Tabs, Tab} from 'material-ui/Tabs'
import TabContent from 'components/TabContent'
import CustomFields from 'components/CustomFields'
import Description from 'components/Description'

type Props = {
  addField: React.PropTypes.func.isRequired,
  changeDescription: React.PropTypes.func.isRequired
};
export class Panel extends React.Component {
  props: Props;
  state = {
    activeTab: 0
  }

  handleTabChange = (index) => {
    this.setState({activeTab: index})
  };

  render () {
    const {addField, changeDescription} = this.props
    return (
      <div className={styles.container}>
        <div className={styles.container_title}>San Francisco Driver Form</div>
        <Tabs
          value={this.state.activeTab}
          onChange={::this.handleTabChange}>
          <Tab
            label={<span className={styles.container_tab}>Custom fields</span>}
            value={0}
          >
            <TabContent
              logo='Add Custom Field'
              desc='Select fields will be added to form'>
              <CustomFields addField={addField} />
            </TabContent>
          </Tab>
          <Tab
            label={<span className={styles.container_tab}>Description(optional)</span>}
            value={1}>
            <TabContent
              logo='Form Description'
              desc='Optional form description'>
              <Description changeDescription={changeDescription} />
            </TabContent>
          </Tab>
        </Tabs>
      </div>
    )
  }
}

export default Panel

