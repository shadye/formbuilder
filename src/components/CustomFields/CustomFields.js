import React from 'react'
import WButton from 'components/WButton'
import styles from './CustomFields.css'

type Props = {
  addField: React.PropTypes.func.isRequired
};
export class CustomFields extends React.Component {
  props: Props

  render () {
    const { addField } = this.props
    const ButtonsConfig = [
      {
        text: 'Single-line text',
        handler: addField.bind(null, {title: '', type: 'text', choices: '', isRequired: false})
      },
      {
        text: 'Radio Button',
        handler: addField.bind(null, {title: '', type: 'radio', choices: [], isRequired: false})
      },
      {
        text: 'Checkboxes',
        handler: addField.bind(null, {title: '', type: 'checkbox', choices: [], isRequired: false})
      },
      {
        text: 'Select',
        handler: addField.bind(null, {title: '', type: 'select', choices: [], isRequired: false})
      },
      {
        text: 'File upload',
        handler: addField.bind(null, {title: '', type: 'file', choices: '', isRequired: false})
      },
      {
        text: 'Paragraph text',
        handler: addField.bind(null, {title: '', type: 'ptext', choices: '', isRequired: false})
      }
    ]
    return (
      <div>
        {ButtonsConfig.map((b, i) => <WButton key={i} label={b.text} handler={b.handler} />)}
      </div>
    )
  }
}

export default CustomFields

