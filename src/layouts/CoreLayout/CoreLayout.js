import React from 'react'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import {white, cyan500} from 'material-ui/styles/colors'
import classes from './CoreLayout.css'

const muiTheme = getMuiTheme({
  palette: {
    // primary1Color: white,
    accent1Color: "#7d88d4"
  },
  tabs: {
    backgroundColor: white,
    textColor: "#d7d7d7",
    selectedTextColor: "#7d88d4"
  },
  flatButton: {
    textColor: "#7d88d4"
  },
  checkbox: {
    checkedColor: "#7d88d4"
  }
});


export const CoreLayout = ({ children }) => (
  <MuiThemeProvider muiTheme={muiTheme}>
    <div className={classes.mainContainer}>
      {children}
    </div>
  </MuiThemeProvider>
)

CoreLayout.propTypes = {
  children: React.PropTypes.element.isRequired
}

export default CoreLayout
