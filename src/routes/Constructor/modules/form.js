import { createAction, handleActions } from 'redux-actions'

// ------------------------------------
// Actions
// ------------------------------------
export const addField = createAction('add field')
export const editField = createAction('edit field')
export const replaceField = createAction('replace field')
export const removeField = createAction('remove field')
export const changeDescription = createAction('change description')

// ------------------------------------
// Action Handlers
// ------------------------------------
const localState = window.localStorage.getItem('redux form')
const initialState = localState ? JSON.parse(localState) : { description: '', fields: [] }
const assignFields = (state, newFields) => {
  return Object.assign({}, state, {fields: newFields})
}

export default handleActions({
  'add field': (state, action) => {
    return assignFields(state, [...state.fields, action.payload])
  },
  'edit field': (state, action) => {
    let field = Object.assign({}, state.fields[action.payload.index], action.payload.delta)
    state.fields.splice(action.payload.index, 1)
    state.fields.splice(action.payload.index, 0, field)
    return assignFields(state, [...state.fields])
  },
  'replace field': (state, action) => {
    let newState = []
    action.payload.map(f => newState.push(state.fields[f]))
    return assignFields(state, newState)
  },
  'remove field': (state, action) => {
    state.fields.splice(action.payload, 1)
    return assignFields(state, [...state.fields])
  },
  'change description': (state, action) => {
    return Object.assign({}, state, {description: action.payload})
  }
}, initialState)
