import { connect } from 'react-redux'
import Constructor from 'components/Constructor'
import {
  addField,
  editField,
  replaceField,
  removeField,
  changeDescription } from '../modules/form'

const mapActionCreators = {
  addField,
  editField,
  replaceField,
  removeField,
  changeDescription
}

const mapStateToProps = (state) => ({
  form: state.form.fields,
  description: state.form.description
})

export default connect(mapStateToProps, mapActionCreators)(Constructor)
